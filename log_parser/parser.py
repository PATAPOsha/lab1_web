import json
import re
from datetime import datetime


class Log:
    def __init__(self,
                 ip,
                 identity,
                 userid,
                 date,
                 request_line,
                 status_code,
                 resp_size,
                 referer,
                 user_agent):
        self.ip = ip
        self.identity = identity
        self.userid = userid
        self.date = datetime.strptime(date, "%d/%b/%Y:%H:%M:%S %z")
        self.request_line = request_line
        self.status_code = status_code
        self.resp_size = resp_size
        self.referer = referer
        self.user_agent = user_agent

        self.http_method, self.url, self.http_ver = self.request_line.split(' ')

    def to_json(self):
        return json.dumps(self.__dict__)

    def to_dict(self):
        return self.__dict__


class LogParser:
    def __init__(self, log_file):
        with open(log_file) as f:
            self.log_lines = f.readlines()

        self.regex = '([(\d\.)]+) - - \[(.*?)\] "(.*?)" (\d+) - "(.*?)" "(.*?)"'

    def parse_logs(self):
        for line in self.log_lines:
            res = tuple(map(''.join, re.findall(r'\"(.*?)\"|\[(.*?)\]|(\S+)', line)))
            log_obj = Log(*res)
            yield log_obj