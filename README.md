# Description

This repo provides tools for parsing apache access logs into python `<class 'log_parser.parser.Log'>` object.

See `log_parser.parser.LogParser` and `log_parser.parser.Log`.

```python
>>> from log_parser import LogParser
>>> parser = LogParser('access.log.txt')
>>> parser
<log_parser.parser.LogParser object at 0x00000000037DA780>
>>> logs = list(parser.parse_logs())
>>> logs[0]
<log_parser.parser.Log object at 0x00000000037CEE48>
```

`log_parser.parser.Log` object cat be serialized to json and converted to python dict:
```python
pprint.pprint(logs[0].to_dict())
{'date': datetime.datetime(2017, 2, 22, 0, 0, 10, tzinfo=datetime.timezone(datetime.timedelta(0, 3600))),
 'http_method': 'GET',
 'http_ver': 'HTTP/1.1',
 'identity': '-',
 'ip': '78.46.24.99',
 'referer': '-',
 'request_line': 'GET /books/part/18623 HTTP/1.1',
 'resp_size': '42614',
 'status_code': '200',
 'url': '/books/part/18623',
 'user_agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 '
               'Firefox/38.0',
 'userid': '-'}

```

After serializing `log_parser.parser.Log` objects can be easily saved to mongo:

```python
# save json serialized Log objects to mongo
client = MongoClient(connect=False)
db = client.logs_db

db.apache_logs.insert_many([x.to_dict() for x in logs])
```

![](Screenshot_1.png)

# Analysis

8214 logs analyzed
##### Found:

* Daily users number: 1047
* Most used OS: Linux - 46.5%
* Bots: 20 unique bots
	* some of them:
```bash
Mozilla/5.0 (compatible; AhrefsBot/5.2; +http://ahrefs.com/robot/):122
Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html):47
Googlebot/2.1 (+http://www.google.com/bot.html):1
Mozilla/5.0 (compatible; YandexAccessibilityBot/3.0; +http://yandex.com/bots):4
```
