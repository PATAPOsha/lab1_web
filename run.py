from log_parser import LogParser, Log
from pymongo import MongoClient


if __name__ == '__main__':

    # parse logs to Log object
    parser = LogParser('access.log.txt')
    logs = list(parser.parse_logs())
    print(logs[0].to_dict())

    # save json serialized Log objects to mongo
    client = MongoClient(connect=False)
    db = client.logs_db

    db.apache_logs.insert_many([x.to_dict() for x in logs])

