import re


# count unique daily users
def daily_u(logs):
    day = re.findall(r'\d{1,2}/\w{1,3}/\d{1,4}', logs)
    s = set(day)
    d = list(s)
    print("Daily users number:")
    for i in range(len(d)):
        cmp = "\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3} - - \[" + d[i]
        cm = re.compile(cmp)
        ip = cm.findall(logs)
        print("{}:{}".format(d[i], len(set(ip))))


# user-agent
def get_useragent_full(logs):
    agent = re.findall(r'" ".+', logs)
    s1 = set(agent)
    a = list(s1)
    # temp = 0
    print("User agent number:")
    for j in range(len(a)):
        print("{}:{}".format(a[j], agent.count(a[j])))
        # temp = temp + agent.count(a[j])


# os
def os_range(readf):
    os = re.findall(r'\([^Kc"ias_crawler"].*?;', readf)
    s2 = set(os)
    o = list(s2)
    sum = 0
    for k in range(len(o)):
        sum = sum + os.count(o[k])
    for k in range(len(o)):
        os_pr = o[k]
        per = round(((os.count(o[k]) * 100) / sum), 2)
        print("{}:{}%".format(os_pr[1:], per))


# bots detected
def bots(logs):
    agent = re.findall(r'" ".+', logs)
    s1 = set(agent)
    a = list(s1)
    b_n = 0
    for j in range(len(a)):
        a_l = a[j].lower()
        is_bot = a_l.find("bot")
        if is_bot > -1:
            b_n = b_n + 1
    print("{} unique bots detected".format(b_n))


if __name__ == '__main__':
    with open('access.log.txt') as f:
        logs = f.read()

    daily_u(logs)
    get_useragent_full(logs)
    os_range(logs)
    bots(logs)
